﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace YellowServer
{
    public class Listener
    {
        public void Start() { }

        public void Stop() { }

        public event Action<IConnection> OnNewConnection;
    }

    public class Worker
    {
        public IHttpAdapter Adapter { get; set; }

        public void Process(IConnection conn)
        {
            var requestData = conn.ReadData();
            var request = this.Adapter.ToRequest(requestData);
            IHttpProcessor processor = CreateProcessor(request);
            var response = processor.Process(request);
            var responseData = this.Adapter.FromResponse(response);
            conn.WriteData(responseData);
        }

        private IHttpProcessor CreateProcessor(HttpRequest request)
        {
            throw new NotImplementedException();
        }
    }

    public interface IConnection
    {
        byte[] ReadData();

        void WriteData(byte[] data);
    }

    public interface IHttpAdapter
    {
        HttpRequest ToRequest(byte[] data);

        byte[] FromResponse(HttpResponse response);
    }

    public interface IHttpProcessor
    {
        HttpResponse Process(HttpRequest request);
    }


    public class HttpRequest
    {
    }

    public class HttpResponse
    {
    }
}
