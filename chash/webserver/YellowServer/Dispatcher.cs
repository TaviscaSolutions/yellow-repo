﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YellowServer.CustomException;

namespace YellowServer
{
    public class Dispatcher
    {
        /// <summary>
        ///     Responsible for storing application level configuration 
        /// </summary>
        private static HandlerConfig _handlerConfig;

        public Dispatcher() {

            if (_handlerConfig == null)
            {
                _handlerConfig = new HandlerConfig();
            }
        }       
        
        public IHttpAdapter Adapter { get; set; }

        public void Dispatch(IConnection connection)
        {
            if (connection == null)
            {
                throw new InvalidConnectionState();
            }

            RequestDetails requestDetailsForValidation = GetMethodTypeAndExpectedResource(connection.ReadData());

            if (requestDetailsForValidation.Method.Equals("GET"))
            {
                if (_handlerConfig.IsHandlerDefined(requestDetailsForValidation.ResourceExtention))
                {
                    new Worker().Process(connection);
                }
                else
                {
                    throw new ResourceHandlerNotDefined();
                }
            }
            else
            {
                throw new MethodTypeNotSupported();
            }
        }

        /// <summary>
        ///  Model to store method type and expected resource extention
        /// </summary>
        private class RequestDetails
        {
            public string Method { get; set; }
            public string ResourceExtention { get; set; }
        }

        /// <summary>
        /// Retrives the method type and resource extention form the requested data in byte
        /// </summary>
        /// <param name="requestBuffer"> this is the requested byte data from the connection object</param>
        /// <returns>returns the RequestDetailsForValidation</returns>
        private RequestDetails GetMethodTypeAndExpectedResource(byte[] requestBuffer)
        {
            var requestDetailsForValidation = new RequestDetails();

            string requestString = Encoding.UTF8.GetString(requestBuffer, 0, requestBuffer.Length);


            /// to find the method type from start of the request string 
            /// Example : requestString contains 'GET /file.aspx HTTP/1.0\nFrom: someuser@jmarshall.com\nUser-Agent: HTTPTool/1.0"'
            /// so httMethodType ='GET', startIndexOfResourcePath = 4 and lengthOfResourcePath = 10
            /// requestUrl = '/file.aspx'
            string httpMethodType = requestString.Substring(0, requestString.IndexOf(" "));

            /// to find the index start index and lenth of resource path
            int startIndexOfResourcePath = requestString.IndexOf(httpMethodType) + httpMethodType.Length + 1;
            int lengthOfResourcePath = requestString.IndexOf("HTTP") - startIndexOfResourcePath - 1;


            string requestedUrl = requestString.Substring(startIndexOfResourcePath, lengthOfResourcePath);

            requestDetailsForValidation.Method = httpMethodType;
            requestDetailsForValidation.ResourceExtention = Path.GetExtension(requestedUrl);

            return requestDetailsForValidation;
        }
    }
}
