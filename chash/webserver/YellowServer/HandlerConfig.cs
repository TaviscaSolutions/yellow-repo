﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YellowServer
{
    public class HandlerConfig
    {
        public IList<Handler> HttpHandlers { get; set; }

        public bool IsHandlerDefined(string resourceExtention)
        {
            if (this.HttpHandlers != null)
            {
                return this.HttpHandlers.Any(x => string.Equals(x.Verb, resourceExtention, StringComparison.InvariantCultureIgnoreCase));
            }
            else
            {
                return false;
            }
        }
    }

    public class Handler
    {
        public String Verb { get; set; }
        public String Path { get; set; }
        public String Type { get; set; }
        public String Assembly { get; set; }
    }
}
