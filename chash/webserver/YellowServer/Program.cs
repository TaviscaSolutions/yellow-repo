﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YellowServer
{
    class Program
    {
        static void Main(string[] args)
        {
            var listener = new Listener();
            listener.OnNewConnection += conn => new Dispatcher().Dispatch(conn);
            listener.Start();            
        }
    }
}
