﻿using System;
using System.Text;
using System.StubHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Simple.Mocking;
using YellowServer;
using YellowServer.CustomException;
using YellowServerFixture;
using System.Collections.Generic;

namespace YellowServerFixture
{
    [TestClass]
    public class DispatcherFixture
    {
        [TestMethod]
        [ExpectedException(typeof(InvalidConnectionState))]
        public void TryConnection()
        {
            IConnection connection = null;
            new Dispatcher().Dispatch(connection);

        }

        [TestMethod]
        [ExpectedException(typeof(MethodTypeNotSupported))]
        public void TryPostRequest()
        {

            var connectionMock = Mock.Interface<IConnection>();

            /// Considering its a POST method request with valid resource extention .html
            string requestString = "POST /file.html HTTP/1.0\nFrom: someuser@jmarshall.com\nUser-Agent: HTTPTool/1.0";

            byte[] requestByteData = Encoding.UTF8.GetBytes(requestString);

            Expect.MethodCall(() => connectionMock.ReadData()).Returns(requestByteData);

            var dispatcher = new Dispatcher();
            dispatcher.Dispatch(connectionMock);

        }
       
        [TestMethod]
        [ExpectedException(typeof(ResourceHandlerNotDefined))]
        public void TryResourceHandlerNotDefined()
        {

            var connectionMock = Mock.Interface<IConnection>();

            /// Considering its a GET method request with invalid resource extention .aspx
            string requestString = "GET /file.aspx HTTP/1.0\nFrom: someuser@jmarshall.com\nUser-Agent: HTTPTool/1.0";

            byte[] requestByteData = Encoding.UTF8.GetBytes(requestString);

            Expect.MethodCall(() => connectionMock.ReadData()).Returns(requestByteData);

            var dispatcher = new Dispatcher();
            dispatcher.Dispatch(connectionMock);

        }

        [TestMethod]
        [ExpectedException(typeof(ResourceHandlerNotDefined))]
        public void TryResourceHandlerDefined()
        {

            var connectionMock = Mock.Interface<IConnection>();

            /// Considering its a GET method request with invalid resource extention .aspx
            string requestString = "GET /file.aspx HTTP/1.0\nFrom: someuser@jmarshall.com\nUser-Agent: HTTPTool/1.0";

            byte[] requestByteData = Encoding.UTF8.GetBytes(requestString);

            Expect.MethodCall(() => connectionMock.ReadData()).Returns(requestByteData);

            var dispatcher = new Dispatcher();
            dispatcher.Dispatch(connectionMock);

        }

    }
}
