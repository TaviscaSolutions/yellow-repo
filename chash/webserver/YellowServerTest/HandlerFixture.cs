﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YellowServer;

namespace YellowServerFixture
{
    [TestClass]
    public class HandlerFixture
    {
        [TestMethod]
        public void TryResourceHandlerDefined()
        {
            var handlerConfig = new HandlerConfig();
            
            var requestedResourceExtention = ".html";
            
            handlerConfig.HttpHandlers = SetHandlers();

            bool handlerDefined =  handlerConfig.IsHandlerDefined(requestedResourceExtention);

            Assert.IsTrue(handlerDefined);
        }

        [TestMethod]
        public void TryResourceHandlerNotDefined()
        {
            var handlerConfig = new HandlerConfig();

            var requestedResourceExtention = ".aspx";

            handlerConfig.HttpHandlers = null;

            bool handlerDefined = handlerConfig.IsHandlerDefined(requestedResourceExtention);

            Assert.IsFalse(handlerDefined);
        }


        /// <summary>
        ///     basic mock data of hadnlers 
        /// </summary>
        /// <returns>handlers defined for few extentions</returns>
        private static List<Handler> SetHandlers()
        {
            var httpHandlers = new List<Handler>();
            var handlerHtml = new Handler();
            handlerHtml.Verb = ".html";
            var handlerJs = new Handler();
            handlerJs.Verb = ".js";
            var handlerCss = new Handler();
            handlerCss.Verb = ".css";
            var handlerDefault = new Handler();
            handlerDefault.Verb = "";

            httpHandlers.Add(handlerHtml);
            httpHandlers.Add(handlerCss);
            httpHandlers.Add(handlerJs);
            httpHandlers.Add(handlerDefault);
            return httpHandlers;
        }

    }
}
