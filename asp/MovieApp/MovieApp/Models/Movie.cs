﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieApp.Models
{
    public class Movie
    {
        public int ID { get; set; }

        public string Title { get; set; }

        public DateTime ReleaseDate { get; set; }

        public string Genre { get; set; }

        public decimal Price { get; set; }

        public string Rating { get; set; }

        public Movie() { 
        
        }

        public Movie(int id, string title, DateTime releaseDate, string genre, decimal price, string rating)
        {
            this.ID = id;
            this.Title = title;
            this.ReleaseDate = releaseDate;
            this.Genre = genre;
            this.Price = price;
            this.Rating = rating;
        }
    }
}