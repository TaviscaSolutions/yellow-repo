﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieApp.Models
{
    public class AccessToken
    {
        public Guid Token { get; set; }
        public DateTime CreatedTimeStamp { get; set; }

        public AccessToken() { 
            
        }

        public AccessToken(Guid token,DateTime createdTimeStamp) {
            this.Token = token;
            this.CreatedTimeStamp = createdTimeStamp;
        }
    }
}