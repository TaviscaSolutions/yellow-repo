﻿using MovieApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApp.Services.IService
{
    public interface IMovieServiceProxy
    {
        void Create(AccessToken accessToken, Movie movie);
        void Update(AccessToken accessToken,Movie movie);
        void Delete(AccessToken accessToken,int movieId);
        List<Movie> FindAll(AccessToken accessToken);
        List<Movie> FindByName(AccessToken accessToken,string title);
        List<Movie> FindByGenre(AccessToken accessToken,string genre);
        List<Movie> Search(AccessToken accessToken, string title, string rating, string genre);
    }
}
