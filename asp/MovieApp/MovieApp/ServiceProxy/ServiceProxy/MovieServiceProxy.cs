﻿using MovieApp.Services.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MovieApp.Models;

namespace MovieApp.Services.Service
{
    public class MovieServiceProxy : IMovieServiceProxy
    {
        public void Create(AccessToken accessToken, Movie movie)
        {
            throw new NotImplementedException();
        }

        public void Update(AccessToken accessToken, Movie movie)
        {
            throw new NotImplementedException();
        }

        public void Delete(AccessToken accessToken, int movieId)
        {
            throw new NotImplementedException();
        }

        public List<Movie> FindAll(AccessToken accessToken)
        {
            throw new NotImplementedException();
        }

        public List<Movie> FindByName(AccessToken accessToken, string title)
        {
            throw new NotImplementedException();
        }

        public List<Movie> FindByGenre(AccessToken accessToken, string genre)
        {
            throw new NotImplementedException();
        }

        public List<Movie> Search(AccessToken accessToken, string title, string rating, string genre)
        {
            throw new NotImplementedException();
        }
    }
}