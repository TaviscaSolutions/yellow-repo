﻿Explorer.Configurations = new (function () {
    this.validCategories = 'Food,Coffee,OutDoors,Drinks,Shops,Arts';
    this.apiBaseUrl = 'http://explore.appacitive.com/locations';
    this.radius = 3;
    this.pageSize = 200;
    this.currentLat = 18.5203;
    this.currentLng = 73.8567;
})();