Explorer.CategoryController = new (function () {

    categoryView = null,

    categories = null,

    //Initializing controller(CategoryController)
    this.init = function () {
        //intialise count to zeros
        this.resetCategories();

        //create object of CategoryView 
        this.categoryView = new Explorer.CategoryView();

        //call render function of category view
        this.categoryView.render(this.categories);
    },

    //Summary
    //  on change function 
    //  output: display count of categories 
    this.onPlaceChange = function () {
        var self = this;

        self.resetCategories();
       
        // iterating over all the models
        // when ever category name matches to filters value
        // increment the category counter and total counter
        _.forEach(Explorer.AppController.places.models, function (place) {
            var filter = _.findWhere(self.categories.filters, { value: place.get('category') });
            if (!filter) return;

            // increment the category counter and total counter
            self.categories.noOfRecords++;
            filter.count++;
        });

        //call render function of category view
        this.categoryView.render(self.categories);
    },

    //Summary
    //  function to initialize all categories attributes

    this.resetCategories = function () {
        var self = this;

        self.categories = {
            noOfRecords: 0,
            filters: [],
        };

        // Initialising the filter array 
        _.forEach(Explorer.Configurations.validCategories.split(','), function (category) {
            self.categories.filters.push({
                name: category,
                count: 0,
                value: category.toLowerCase()
            });
        });
    }
})();