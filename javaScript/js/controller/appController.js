Explorer.AppController = new (function () {
    //Initialising the collection places

    this.places = null,
    this.currentLocation = null;

    /**
     * Initializing :CategoryController , MapController
     * call appView.render      
     * @return {none}
     */
    this.init = function () {
        //Initialising the category Controller
        Explorer.CategoryController.init();

        //Initialising the map Controller
        Explorer.MapController.init();

        //Initialising the FB Controller
        Explorer.FbController.init();

        // intialize places collection
        this.places = new Explorer.Places();

        // when ever any place gets added to the collection
        // show marker will get call
        // this will be done by the backbone 'add' event
        this.places.bind('add', Explorer.MapController.showMarker, Explorer.MapController);
        
        //call to onPlaceChangeEvent method on change of search query	   
        this.places.bind('placesAdded reset', Explorer.CategoryController.onPlaceChange, Explorer.CategoryController);

        //binding the triggers for input boxes 
        this.places.bind('reset', Explorer.AppView.textBoxDisabled, this);
        this.places.bind('fetched', this.textBoxEnabled, this);
        // Instantiating AppView
        this.appView = new Explorer.AppView();

        // Call to render function of appView for rendering searched location
        this.appView.render();

        this.currentLocation = new Explorer.Location();

        // Call to setMapCenter method on change of search
        this.currentLocation.on('change', Explorer.MapController.setMapCenter, Explorer.MapController);

        //Setting the default location to pune 
        this.setCurrentLocation(Explorer.Configurations.currentLat, Explorer.Configurations.currentLng);
    },

    /**
     * [setCurrentLocation description : 
     *  sets the currentLocation object attributes
     * ]
     * @param {[type]} latitude
     * @param {[type]} longitude
     */
    this.setCurrentLocation = function (latitude, longitude) {
        this.currentLocation.set({ latitude: latitude, longitude: longitude });
        this.getPlaces(this.currentLocation);
    },

    /**
     * [getPlaces description : 
     * Firstly remove all markers from map for re-rendering
     * Then calls the fetch function for getting the collection
     * ]
     * @param  {[type]} location
     * @param  {[type]} filter
     * @return {[none]}
     */
    this.getPlaces = function (location, filter) {
        location = location || this.currentLocation;

        //remove all the markers from map when loading new location
        Explorer.MapController.removeMarkers();
        // Call to fetch method for fetching searched position
       
        this.places.fetch({ location: location, filterValue: filter });
    }  
       

})();