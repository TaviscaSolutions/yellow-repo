Explorer.LoaderController = new (function () {

    //Initializing controller(LoaderController)
    this.init = function () {

        // Instantiating LoaderView
        var load = new Explorer.LoaderView();
       
        // Call to a function named showLoaderView in loaderView on beginning of ajax call
        this.showLoad = $(document).ajaxStart(function () {
            load.showLoaderView();
         
        });

        // Call to a function named hideLoaderView in loaderView on end of ajax call
        this.hideLoad=$(document).ajaxStop(function () {
            load.hideLoaderView();
        });
    }

})();
