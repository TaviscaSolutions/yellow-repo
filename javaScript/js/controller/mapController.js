Explorer.MapController = new (function () {
 
    mapView: null,   
    
    /**
     * [init description : Initializing controller(MapController)]
     * @return {[none]}
     */
    this.init = function () {
        //copy namespace of google.maps to Explorer.Map for reference
        Explorer.Map = google.maps;

        //create MapView object and render Map
        this.mapView = new Explorer.MapView();
        this.mapView.render();
        // this.mapView.showMarkers

    },

    /**
     * [setMapCenter description : set center of map to new location]
     * @param {[object]} mapCenterLocation
     */
    this.setMapCenter = function (mapCenterLocation) {
        this.mapView.setMapCenter(mapCenterLocation);
    },

       /**
     * [showMarker description : set the marker on map]
     * @param  {[type]} place
     * @return {[none]}
     */
    this.showMarker = function (place) {
        this.mapView.placeMarkerOnMap(place);
        $('#txtSearch').removeAttr('disabled');
    },
     

    /**
     * [removeMarkers description : Deletes all the markers from map]
     * @return {[none]}
     */
    this.removeMarkers = function () {
        this.mapView.removeMarkers();
    },

    //hide marker according to category
    this.hideMarkers = function (category) {
        //Call to a method in Map controller for hiding markers
        this.mapView.hideCategoryMarkers(category);
    },

    this.showMarkers = function (category) {
        //Call to a method in Map controller for showing markers
        this.mapView.showCategoryMarkers(category);
    }

})();