﻿Explorer.FbController = new (function () {

    //Initializing controller(FBController)
    this.init = function () {

        //Initializing View(FBView)
        var fbView = new Explorer.FbView();        
        //Calling Init method of FbView
       fbView.init();
    }

})();