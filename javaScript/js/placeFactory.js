﻿
Explorer.PlaceFactory = new (function () {
    this.createPlace = function (category) {

        //variable used for returning
        var place = null;

        //Switch case for selecting category and instantiating respective objects
        switch (category) {
            case 'arts': place = new Explorer.Arts();   
               
                break;

            case 'coffee': place = new Explorer.Coffee();
                break;

            case 'drinks': place = new Explorer.Drinks();
                break;

            case 'food': place = new Explorer.Food();
                break;

            case 'outdoors': place = new Explorer.Outdoors();
                break;

            case 'shops': place = new Explorer.Shops();
                break;
          default:
                throw "No category recieved";

        }

        return place;
    };

})();