//Defining global namespace
window.Explorer = {};

Explorer.AppRouter = Backbone.Router.extend({

    routes: {

        // Setting default route.
    	"*actions": "defaultRoute"
    }
});


$(function(){
	// Instantiating an AppRouter object
	Explorer.appRouter = new Explorer.AppRouter;

	// Providing a default route
	Explorer.appRouter.on('route:defaultRoute', function (actions) {

        // Calling the init function of AppController
        Explorer.AppController.init();
    });


    // For routing the initial URL.
	Backbone.history.start();



})
