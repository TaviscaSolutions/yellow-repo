Explorer.MapView = Backbone.View.extend({

    //to store map settings
    mapOptions: null,
    //global map variable used by all functions
    map: null,

    infowindow: null,
    //used by hideCategoryMarkers and showCategoryMarkers functions
    markersArray: [],

    //share the same infowindow instance for all infowindows
    //instead of creating new infowindow instances everytime
    infowindow: null,

    //shared by reder and setMapCenter functions
    //using global to remove the previously loaded user's location marker from map
    userLocationMarker: null,

    /**
     * [render description : called when Explore page loads,
     * displays the Google Map on Explore page]
     * @return {none}
     */

    render: function () {
        var self = this;

        // to store user's initial location
        var initialLocation;

        //to check browser's compatibility for Geolocation
        var browserSupportFlag = new Boolean();

        //assume user's initial location to pune if navigator fails
        //works as default (fallback for navigator.geolocation)
        var puneGeoLocation = new Explorer.Map.LatLng(Explorer.Configurations.currentLat, Explorer.Configurations.currentLng);

        // Setting the position of MapControls
        self.mapOptions = {
            center: new google.maps.LatLng(Explorer.Configurations.currentLat, Explorer.Configurations.currentLng),
            zoom: 10,
            //Set the panControl button to right
            panControl: true,
            panControlOptions: {
                position: google.maps.ControlPosition.TOP_RIGHT
            },
            //Set the zoomControl button to right
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.LARGE,
                position: google.maps.ControlPosition.TOP_RIGHT
            }
        };

        //get the map container from HTML DOM  
        self.map = new Explorer.Map.Map(document.getElementById("mapView"),
            self.mapOptions);

        //if browser supports navigator.geolocation
        if (navigator.geolocation) {
            browserSupportFlag = true;
            navigator.geolocation.getCurrentPosition(function (position) {
                initialLocation = new Explorer.Map.LatLng(position.coords.latitude, position.coords.longitude);
                self.map.setCenter(initialLocation);
            }, function () {
                handleNoGeolocation(browserSupportFlag);
            });
        }
            // Browser doesn't support Geolocation
        else {
            browserSupportFlag = false;
            handleNoGeolocation(browserSupportFlag);
        }

        //handle navigator fail condition
        function handleNoGeolocation(errorFlag) {
            if (errorFlag == true) {
                //console.log("Geolocation service failed. We've placed you in Pune.");
                initialLocation = puneGeoLocation;
            } else {
                //console.log("Your browser doesn't support geolocation. We've placed you in Pune.");
                initialLocation = puneGeoLocation;
            }
            self.map.setCenter(initialLocation);
        }

        var image = 'images/Markers/marker_userlocation.png';
        var icon = null;

        //show the user's location marker
        userLocationMarker = new Explorer.Map.Marker({
            position: puneGeoLocation,
            map: self.map,
            animation: Explorer.Map.Animation.DROP,
            icon: image
        });

    },

    /**
     * [placeMarkerOnMap description : sets a marker on map at location 'place'
     * adds infoWindow on click event on marker
     * ]
     * @param  {object} place
     * @return {none}
     */
    placeMarkerOnMap: function (place) {
        self = this;
        var image;
        infowindow = new Explorer.Map.InfoWindow();
        var latlng = new Explorer.Map.LatLng(place.location.get('latitude'),
            place.location.get('longitude'));

        image = place.getIconUrl();
        var icon = null;
        var marker = new Explorer.Map.Marker({
            position: latlng,
            map: self.map,
            icon: image,

        });

        Explorer.Map.event.addListener(marker, 'click', (function (marker) {
            return function () {
                infowindow.close();
                var $source = $("#infoWindowTemplate").html();
                var template = Handlebars.compile($source);
                var $placeholder = $('.overlay');
                var serverImage = place.get('images');
                var category = place.get('category');
                var imagePath;
                if (serverImage[0] == null || serverImage[0] == '') {
                    imagePath = 'images/' + category + '-icon.png';
                } else {
                    imagePath = serverImage[0];
                }
                var context = {
                    'name': place.get('name'),
                    'address': place.get('description'),
                    'street': place.address.get('street'),
                    'cityState': place.address.get('city') + ',' + place.address.get('country'),
                    'photo': imagePath
                };
                var html = template(context);
                infowindow.setContent(html);
                $placeholder.append(infowindow.open(self.map, marker));
                var borderShow = $('.overlay').parent().parent().parent().siblings();
                borderShow.addClass('parent-InfoBox');


            }
        })(marker));
        self.markersArray.push(marker);
    },



    /**
     * [HideCategoryMarkers description : hides all 'hideCategory' type of markrs in 'places']
     * @param {[type]} hideCategory
     * @param {[type]} places
     */
    hideCategoryMarkers: function (hideCategory) {
        _.forEach(Explorer.AppController.places.models, function (place, i) {
            if (place.get('category') === hideCategory) {
                self.markersArray[i].setVisible(false)
            }
        });
    },


    /**
     * [ShowCategoryMarkers description : shows markers of 'showcategory' type in 'places']
     * @param {[type]} showcategory
     * @param {[type]} places
     */
    showCategoryMarkers: function (showcategory) {
        _.forEach(Explorer.AppController.places.models, function (place, i) {
            if (place.get('category') === showcategory) {
                self.markersArray[i].setVisible(true);
            }
        });
    },

    /**
     * [setMapCenter description: set the center the the map to 'mapCenterLocation']
     * @param {[type]} mapCenterLocation
     */
    setMapCenter: function (mapCenterLocation) {
        userLocationMarker.setMap(null);
        newLocation = new Explorer.Map.LatLng(mapCenterLocation.get('latitude'), (mapCenterLocation.get('longitude')));
        var image = 'images/Markers/marker_userlocation.png';
        userLocationMarker = new Explorer.Map.Marker({
            position: newLocation,
            map: this.map,
            icon: image,
            animation: Explorer.Map.Animation.DROP,
        });
        this.map.setCenter(newLocation);
        this.map.setZoom(15);
    },


    /**
     * [removeMarkers description : delete all the markers from map]
     * @return {[type]}
     */
    removeMarkers: function () {

        self = this;
        for (var i = 0; i < self.markersArray.length; i++) {
            self.markersArray[i].setMap(null);
        }
        self.markersArray.length = 0;

    }

});