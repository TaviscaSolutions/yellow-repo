Explorer.CategoryView = Backbone.View.extend({
    el: '#results',
    hbTemplate: null,
    events: {

        // Event for tracking the click on respective categories
        'click div.map-geocoder': 'onCategoryClick',
        'click div.map-geocoder *': 'onCategoryClick'
    },

    //Summary
    //  onclick of category function check if it is        
    //  active or not 
    //  Accordingly call to hide or show markers


    onCategoryClick: function (e) {
        var $ele = $(e.target);

        // check if the child element is clicked
        // if yes then take the parent as $ele
        if ($ele.hasClass('map-geocoder') === false)
            $ele = $ele.parent();

        // stop the propagation of the event to parent handler
        e.stopPropagation();

        var categoryName = $ele.find('.category').text().toLowerCase();
        //for matching with ajax data which is in lowercase,category name is in lower case

        if ($ele.hasClass('active')) {
            //call to function in Map controller for hiding respective markers
            Explorer.MapController.hideMarkers(categoryName);

        }
        else {
            //call to function in Map controller for showing respective markers
            Explorer.MapController.showMarkers(categoryName);
        }
        //for toggling between selection and deselection of categories 
        $ele.toggleClass('active');
    },



    //Summary
    // Compile a template in JavaScript
    // Calling renderCategoryPanel  
    render: function (categories) {
        this.hbTemplate = Handlebars.compile($("#categoryListTemplate").html());
        this.renderCategoryPanel(categories);

    },

    //Summary
    //  Reset data with new data
    //  Attaching template to DOM and rendering data
    renderCategoryPanel: function (categories) {
        this.$el.empty();
        this.$el.html(this.hbTemplate(categories));

    }
});


