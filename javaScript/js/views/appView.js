
Explorer.AppView = Backbone.View.extend({
    
    // To access DOM elements in backbone for event handling
    el: '#divWrapper',

    //define events that this element will listen to
    events: {
        'keyup #txtSpecificSearch': 'filterSearch',
        'click #lnkSetCurrent': 'setCurrentPosition'
    },

   
    /**
     * [render description : it gets the latitude and longitude of the searched place ]
     * @return {[none]}
     */
    render: function () {
        var input = document.getElementById('txtSearch');
        var self = this;
        self.trigger("resetCountEvent");
        Explorer.AppController.places.bind('resetCountEvent', Explorer.CategoryView.resetCategoryCount, Explorer.CategoryView);
        var options = {

            /* Sets the types of predictions to be returned. Supported types are 'establishment' for businesses and 'geocode' for addresses. 
	    	If no type is specified, both types will be returned. The setTypes method accepts a single element array.*/
            types: ['geocode']
        };

        var autocomplete = new google.maps.places.Autocomplete(input, options);

        Explorer.Map.event.addListener(autocomplete, 'place_changed', function () {

            //Returns the details of the Place selected by user
            var place = autocomplete.getPlace();
            if (place.geometry !== undefined) {
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
                Explorer.AppController.setCurrentLocation(latitude, longitude);
            }
            else {
                alert("Place not found please enter appropriate place!");
                $('#txtSearch').val("");
            }
            
        });

    },

    /**
     * [filterSearch description : passes the filter value to the fetch function]
     * @param  {[event]} e
     * @return {[none]}
     */
    filterSearch: function (e) {
        if (e.keyCode === 13) {
            var filter = $("#txtSpecificSearch").val();
            Explorer.AppController.getPlaces(undefined, filter);

        }
    },
    /**
    For disabling the textbox while the data is fetched
    */
    textBoxDisabled: function () {
        $("input").attr('disabled', 'disabled');
       
    },
    textBoxEnabled : function () {
        $("input").removeAttr('disabled');
        
    },
    /**
    function Description : Sets the current user's location to Pune 
    return type : none 
    **/
    setCurrentPosition: function () {
        Explorer.AppController.setCurrentLocation(Explorer.Configurations.currentLat, Explorer.Configurations.currentLng);
    }
});





