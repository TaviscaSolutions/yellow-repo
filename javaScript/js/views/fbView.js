﻿
Explorer.FbView = Backbone.View.extend({

    //Summary:
    //  Initialising variables

    //Attributes:
    // appId-Application Id
    // xfbml-Extended Facebook MarkUp Language
    // version-xfbml version
    init: function(){
        appId: '1459429257665469';
        xfbml: true;
        version: 'v2.0';
       
        //Summary
        //  For Loading SDK Asynchronously
        (function (d, s, id) {

            //Finding parent by using the script element
            var js, fjs = d.getElementsByTagName(s)[0];

            //Condition for checking if sdk is already installed
            if (d.getElementById(id)) 
                return;

            //Creating new sdk element
            js = d.createElement(s);

            //Setting id of sdk element
            js.id = id;

            //Setting source of new element to source of FB JS SDK
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=1459429257665469&version=v2.0";

            //Inserting source in DOM
            fjs.parentNode.insertBefore(js, fjs);

        }(document, 'script', 'facebook-jssdk'));

     }
 
});

