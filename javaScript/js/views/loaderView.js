Explorer.LoaderView = Backbone.View.extend({

    //Summary
    //  Function for Displaying the Loader 
    showLoaderView: function () {

        //Extracting the loader from html
        $("#imgLoad").show();
    },

    //Summary
    //  Function for Hiding the Loader 
    hideLoaderView: function () {

        // Extracting the loader from html
        $("#imgLoad").hide();
    }
});