Explorer.Place = Backbone.Model.extend({

    // Setting default values for attributes for model:places
    defaults: {
        name: 'default name',
        description: 'default desc',
        category: 'default category',
        images: 'default images'
    },
    // object of model:location as a property of model:place
    location: null,

    // object of model:address as a property of model:place
    address: null,

    // abstract function for getting URL of icon of categories
    getIconUrl: function () { }

});

Explorer.Places = Backbone.Collection.extend({

    url: '',
    model: Explorer.Place,

    // Summary:
    //   Fetching the object from server and storing in collection objects.
    fetch: function (options) {

        options = options || {};
        options.pnum = options.pnum || 1;

        // URL for fetching particular records

        var url = Explorer.Configurations.apiBaseUrl
           + '?lat='
           + options.location.get("latitude")
           + '&lng='
           + options.location.get("longitude")
           + '&rad='
           + Explorer.Configurations.radius
           + '&pnum='
           + options.pnum
           + '&psize='
           + Explorer.Configurations.pageSize;

        if (options.filterValue)
            url = url + '&q=' + options.filterValue;

        var self = this;

        // Resting the collection after every search
        if (options.pnum === 1) {
            self.reset();
        }

        // making an ajax call for getting object as per the query provided
        $.ajax({

            url: url,
            type: 'GET',
            success: function (locations) {
                var places = [];
                Explorer.LoaderController.init();

                // Iterating through the collection for storing individual objects
                _.forEach(locations.location, function (data) {

                    //Instantiating models
                    var location = new Explorer.Location(),
                        address = new Explorer.Address(),
                        place = Explorer.PlaceFactory.createPlace(data.category);

                    // Setting attributes and properties of place object
                    place.set('name', data.name);
                    place.set('description', data.description);
                    place.set('category', data.category);
                    place.set('images', data.images);

                    // Setting attributes and properties of location object
                    var split = data.location.split(',');
                    location.set('latitude', split[0]);
                    location.set('longitude', split[1]);

                    // Extracting the latitude and longitude from the location property 
                    place.location = location;

                    // Setting attributes and properties of address object
                    address.set('street', data.street);
                    address.set('city', data.city);
                    address.set('country', data.country);
                    place.address = address;

                    //Pushing values to the places collection
                    places.push(place);
                });

                // Setting the whole collection at once
                self.add(places);

                //Event triggered when the whole collection is fetched 
                self.trigger("placesAdded");

                // Condition for checking if all records are fetched
                if (locations.pi.totalrecords > self.models.length) {
                    // Recursively calling fetch for each record and incrementing page number
                    ++options.pnum;
                    self.fetch(options);
                    //triggering the event for disabling the textboxes 
                    self.trigger("inputsDisabled");
                }
                else {
                   //triggering the event for enabling the textboxes 
                    self.trigger("fetched");
                }
            },
            error: function (e) {
              
                alert ("Could not load the data ");
            }

        })
    }
});
