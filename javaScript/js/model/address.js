﻿Explorer.Address = Backbone.Model.extend({
	
    // Initialising attributes to default values
    defaults: {
        street: 'default street',
        city: 'default city',
        country: 'default country',       
    }

});