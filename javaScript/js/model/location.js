﻿Explorer.Location = Backbone.Model.extend({

    // Initialising attributes to default values
    defaults: {
        latitude: 0,
        longitude: 0
    }
 
});